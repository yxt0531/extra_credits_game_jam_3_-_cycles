extends Node

var gui # GUI.gd

var fpsCamera # Camera.gd

var bedroom # BedroomController.gd
var bedroomScene # BedroomScene.gd

var livingRoom # LivingRoomController.gd
var livingRoomScene # LivingRoomScene.gd

var kitchen # KitchenController.gd
var kitchenScene # KitchenScene.gd

var bathroom # BathroomController.gd
var bathroomScene # BathroomScene.gd

var helpState # use to record where if is the help is displayed before, 0 == never displayed

func _ready():
	OS.set_window_position((OS.get_screen_size() - OS.get_window_size()) / 2)
	print("window centered")
	helpState = 0
