extends Node

# inventory variables
var gun
var bullet

# state variables
var cab_open
var bullet_found
var toilet_open
var door_open

func _ready():
	# variables init
	gun = 0
	cab_open = 0
	bullet_found = 0
	bullet = 0
	toilet_open = 0
	door_open = 0

func refreshAnimations():
	if cab_open != 0:
		Global.bathroom.playAnimation("open_cabinet")
	if toilet_open != 0:
		Global.bathroom.playAnimation("open_toilet")
	if door_open != 0:
		Global.bathroom.playAnimation("open_door")
	
func getScript(input):
	print(input) # for debug purpose
	
	if input == "":
		return "You are now standing in the centre of the bathroom. This small enclosed space and lacking of any window makes you feel a little bit claustrophobic."
	if input == "look around":
		return "A medicine cabinet, a sink, a toilet seat and a tub, there really isn't anything interesting here."
	if input == "look at room":
		return "It's a simple bathroom but fully equipped."
	
	if input == "look at sink":
		return "A typical ceremic sink."
	if input == "pick up sink":
		return "It is firmly planted into the wall, no ordinary human can possibly able to pull it out."
	if input == "open sink" || input == "open tap" || input == "open water tap":
		return "Water slowly dripping out of the tap."
		
	if input == "look at cabinet" && cab_open == 0:
		return "A small cabinet used to store various personal hygiene related items."
	if input == "open cabinet" || (input == "look at cabinet" && cab_open == 1):
		if cab_open == 0:
			Global.bathroom.playAnimation("open_cabinet")
		cab_open = 1
		bullet_found = 1
		return "You found some .357 round."
	if input == "go to cabinet":
		return "You won't fit."
	
	if input == "look at bullet" && bullet_found == 1:
		return "It can be used with a revolver."
	if input == "pick up bullet" && bullet_found == 1:
		gun = StoryLivingRoom.getInventory("gun")
		if gun == 0:
			return "You don't have a gun, there is no need to pick them up."
		if gun == 1:
			bullet = 1
			return "You loaded the round in your revolver."
			
	if input == "look at toilet seat" || input == "look at toilet":
		return "It's a toilet seat."
	if input == "open toilet seat" || input == "open toilet cap" || input == "open toilet":
		if toilet_open == 0:
			Global.bathroom.playAnimation("open_toilet")
		toilet_open = 1
		return "..."
		
	if input == "look at tub" || input == "look at bathtub":
		return "A tub big enough to fit one body."
	if input == "get in tub" || input == "get in bathtub":
		return "There is no need to take a bath."
		
	if input == "look at door" || input == "look at kitchen door":
		return "A door that goes back to the kitchen."
	if input == "open door" || input == "open kitchen door":
		if door_open == 0:
			Global.bathroom.playAnimation("open_door")
		door_open = 1
		return "Kitchen door opened."
		
	
	# end game options
	if input == "shoot myself" || input == "pull trigger" || input == "shoot my head" || input == "use gun":
		if gun == 1 && bullet == 1:
			Story.triggerEnding()
			return "You put the gun against your head, take a deep breath, slowly close your eyes, and pull the trigger.\n#wallpaintdecoration"
	
	# scene change options
	if input == "go to kitchen":
		if door_open == 1:
			Story.setLocation("kitchen")
			get_tree().change_scene("res://Scenes/KitchenScene.tscn")
		else:
			return "Kitchen door is currently closed."
		
	# invalid input
	return "You don't know what that means..."
