extends Node

# inventory variables
var cable
var scissor
var pen_paper
var duct_tape
var food_bag

# state variables
var drawer_1
var drawer_2
var drawer_3
var door

func _ready():
	# variables init
	cable = 0
	drawer_1 = 0
	drawer_2 = 0
	drawer_3 = 0
	scissor = 0
	pen_paper = 0
	duct_tape = 0
	food_bag = 0
	door = 0
	
func refreshAnimations():
	if drawer_1 != 0:
		Global.bedroom.playAnimation("open_drawer_1")
	if drawer_2 != 0:
		Global.bedroom.playAnimation("open_drawer_2")
	if drawer_3 != 0:
		Global.bedroom.playAnimation("open_drawer_3")
	if door != 0:
		Global.bedroom.playAnimation("open_door")
		
func getInventory(varName):
	if varName == "cable":
		return cable
	elif varName == "scissor":
		return scissor
	elif varName == "duct_tape":
		return duct_tape
	elif varName == "food_bag":
		return food_bag
	else:
		return 0
	
func getScript(input):
	print(input) # for debug purpose
			
	if input == "":
		Global.bedroomScene.moveCamera(1)
		return "You just woke up on a bed without knowing what time or date it is, you feel like your head is filled with lead. The smell of the room makes you sick, it is completely dark outside, nothing can be seen from the window."
	if input == "look around":
		return "There is a bookshelf, a file cabinet, a table with nothing on it, a bed you are currently sitting on, and a door which leads to probably the living room."
	if input == "look at table":
		return "Nothing particularly interesting catches your eye, it is a table you used to work in the old days."
		
	if input == "look at cabinet" || input == "look at file cabinet":
		Global.bedroomScene.moveCamera(2)
		return "It is a normal office file cabinet with three drawers. Not sure what is in it now."
	if input == "open cabinet" || input == "open file cabinet":
		Global.bedroomScene.moveCamera(2)
		return "Which drawer do you want to open?"
		
	if input == "open first drawer" && cable == 0:
		Global.bedroomScene.moveCamera(2)
		if drawer_1 == 0:
			Global.bedroom.playAnimation("open_drawer_1") # play first drawer animation
		drawer_1 = 1 # first drawer opened
		return "It has bunch of cables, and some manuals for the stuff you brought."
	if input == "open first drawer" && cable == 1:
		Global.bedroomScene.moveCamera(2)
		return "Nothing interesting left."
	
	if drawer_1 == 1: # if first drawer is opened
		if (input == "look at cable" || input == "look at cables") && cable == 0:
			Global.bedroomScene.moveCamera(2)
			return "It's a bunch of braided HDMI cables, looks unnecessary sturdy for some cables."
		if (input == "look at cable" || input == "look at cables") && cable == 1:
			Global.bedroomScene.moveCamera(2)
			return "It's on your neck now."
		if (input == "pick up cable" || input == "pick up cables") && cable == 0:
			Global.bedroomScene.moveCamera(2)
			cable = 1
			return "You are holding bunch of cables in your hand, but it seems a bit inconvenient, you decide to put them around your neck."
		if (input == "pick up cable" || input == "pick up cables") && cable == 1:
			Global.bedroomScene.moveCamera(2)
			return "It's on your neck now."
			
		if input == "look at manual" || input == "look at manuals":
			Global.bedroomScene.moveCamera(2)
			return "<PlayStation User Manual>\n<ASUS B350-I Manual>\n..."
		if input == "pick up manual" || input == "pick up manuals":
			Global.bedroomScene.moveCamera(2)
			return "You picked them up, but without knowing what to do with them, you put them back to where they were."
	
	if input == "open second drawer" && (scissor == 0 || pen_paper == 0 || duct_tape == 0):
		Global.bedroomScene.moveCamera(2)
		if drawer_2 == 0:
			Global.bedroom.playAnimation("open_drawer_2") # play second drawer animation
		drawer_2 = 1
		return "There are some video game discs, a pair of scissor, a duct tape and some pen and paper."
	if input == "open second drawer" && scissor == 1 && pen_paper == 1 && duct_tape == 1:
		Global.bedroomScene.moveCamera(2)
		drawer_2 = 1
		return "Nothing interesting."
		
	if drawer_2 == 1:
		if input == "look at scissor":
			Global.bedroomScene.moveCamera(2)
			return "It has a blue handle, blade looks sharp enough."
		if input == "pick up scissor" && scissor == 0:
			Global.bedroomScene.moveCamera(2)
			scissor = 1
			return "You put the scissor in your pocket."
		if input == "pick up scissor" && scissor == 1:
			Global.bedroomScene.moveCamera(2)
			return "You already have it."
			
		if input == "look at duct tape":
			Global.bedroomScene.moveCamera(2)
			return "It's a duct tape, duh."
		if input == "pick up duct tape" && duct_tape == 0:
			Global.bedroomScene.moveCamera(2)
			duct_tape = 1
			return "You put the duct tape in your pocket."
		if input == "pick up duct tape" && duct_tape == 1:
			Global.bedroomScene.moveCamera(2)
			return "You already have it."
		
		if input == "look at pen and paper":
			Global.bedroomScene.moveCamera(2)
			return "It's from the company you used to work for, good for writing letters and such."
		if input == "pick up pen and paper" && pen_paper == 0:
			Global.bedroomScene.moveCamera(2)
			pen_paper = 1
			return "You are holding a pen and a bunch of paper."
		if input == "pick up pen and paper" && pen_paper == 1:
			Global.bedroomScene.moveCamera(2)
			return "You already have them."
			
	if (input == "open third drawer" || input == "open last drawer") && food_bag == 0:
		Global.bedroomScene.moveCamera(2)
		if drawer_3 == 0:
			Global.bedroom.playAnimation("open_drawer_3") # play third drawer animation
		drawer_3 = 1
		return "It has some vacuum food bags, a pack of AA batteries, and a video game controller."
	if (input == "open third drawer" || input == "open last drawer") && food_bag == 1:
		Global.bedroomScene.moveCamera(2)
		return "Nothing interesting."
	
	if drawer_3 == 1:
		if (input == "look at food bag" || input == "look at food bags") && food_bag == 0:
			Global.bedroomScene.moveCamera(2)
			return "It'a special type of bag for Sous-vibe cooking, but it seems quite big for food though."
		if (input == "look at food bag" || input == "look at food bags") && food_bag == 1:
			Global.bedroomScene.moveCamera(2)
			return "You already have them."
		if (input == "pick up food bag" || input == "pick up food bags") && food_bag == 0:
			Global.bedroomScene.moveCamera(2)
			food_bag = 1
			return "You now have a food bag, but you don't seem to have any food."
		if (input == "pick up food bag" || input == "pick up food bags") && food_bag == 1:
			Global.bedroomScene.moveCamera(2)
			return "You already picked them up, still no food though."
		
		if input == "look at battery" || input == "look at batteries":
			Global.bedroomScene.moveCamera(2)
			return "Looks just like regular AA batteries."
		if input == "look at controller":
			Global.bedroomScene.moveCamera(2)
			return "Looks like a controller for PS4, a tiny bit of sense of joy went through your heart."
		if input == "pick up battery" || input == "pick up batteries" || input == "pick up controller":
			Global.bedroomScene.moveCamera(2)
			return "You picked them up, but without knowing what to do with them, you put them back to where they were."
	
	if input == "look at bookshelf":
		Global.bedroomScene.moveCamera(1)
		return "<Thirteen Reasons Why>\n<All the Bright Places>\n<Final Fantasy XV Prima Guide>\n..."
	if input == "read Thirteen Reasons Why" || input == "read All the Bright Places" || input == "read book" || input == "read books" || input == "read":
		Global.bedroomScene.moveCamera(1)
		return "You are not in the mood of reading a book."
		
	if (input == "use food bag" || input == "use food bags") && food_bag == 1 && duct_tape == 0:
		return "You put the food bag on your head, nothing happens."
	if input == "use pen and paper" && pen_paper == 1:
		return "You wrote a letter to your parents, told them that you loved them, and you are sorry about everything, then you scrapped everything, you feel it is pointless to write any letter to anyone."
	if input == "write letter" && pen_paper == 1:
		return "You wrote a letter to your parents, told them that you loved them, and you are sorry about everything, then you scrapped everything, you feel it is pointless to write any letter to anyone."
		
	# other options
	if input == "look at window" || input == "look at windows":
		Global.bedroomScene.moveCamera(1)
		return "It is completely dark outside, you can't see anything but your own reflection on the window."
	if input == "open window" || input == "open windows":
		Global.bedroomScene.moveCamera(1)
		return "It is locked, and you don't feel like to open it anyway."
	if input == "look at bed":
		Global.bedroomScene.moveCamera(1)
		return "A twin-sized bed, nothing fancy about it."
	if input == "look at room":
		return "It is your bedroom, it seems too familier that it makes you sick a little."
	if input == "look at door" || input == "look at living room door":
		return "A white wooden door."
	if input == "open door" || input == "open living room door":
		if door == 0:
			Global.bedroom.playAnimation("open_door") # play open door animation
			door = 1
		return "The bedroom door is now opened, you can see your living room."
	if input == "go to living room" && door == 0:
		return "The door to your living room is currently shut."
	
	if input == "use scissor" && scissor == 1:
		return "You have't decided what to cut."
	
	# scene change options
	if input == "go to bed":
		Global.bedroomScene.moveCamera(1)
		Story.nextCycle()
		return "You go back to bed and decide to sleep even though you just woke up minutes ago."
	if input == "go to living room" && door == 1:
		# proceeed to living room
		Story.setLocation("living room")
		get_tree().change_scene("res://Scenes/LivingRoomScene.tscn")
		return "# change scene to living room"

	# end game options
	if (input == "cut my wrist" || input == "cut wrist") && scissor == 1:
		Story.triggerEnding()
		return "The blade carved deeply into your flesh, an immense feel of pain run from your arm to your body, you gradually start to lose your vision, and eventurally, the pain goes away as well.\n#roseisred_skinisblue"
	if (input == "use food bag" || input == "use food bags") && food_bag == 1 && duct_tape == 1:
		Story.triggerEnding()
		return "You put the bag on your head, and try your best to wrap your neck use the duct tape as much as you can, then the lack of oxygen makes you unable to hold anything, it feels like a charcoal in burning inside your throat, you start to hear a screaming noise, it is so loud that you cannot think of anything, but eventurally, everything is quiet down again.\n#abreathtakingexperience"

	return "You don't know what that means..."
