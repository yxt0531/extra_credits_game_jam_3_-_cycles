extends Camera

onready var Yaw = get_parent()

export var FOVZoom = 50
export var FOVDefault = 100

var mouseSensitivity
var state # if camera is currently zooming, state == 1

func _ready():
	mouseSensitivity = 0.25
	state = 0
	set_process_input(true)
	Global.fpsCamera = self

func _process(delta):
	if state == 0: # zooming back if state == 0
		if fov < FOVDefault:
			setFOV(fov + delta * 100)

func look_updown_rotation(rotation = 0):
	var toReturn = self.get_rotation() + Vector3(rotation, 0, 0)
	toReturn.x = clamp(toReturn.x, PI / -2, PI / 2)

	return toReturn

func look_leftright_rotation(rotation = 0):
	return (Yaw.get_rotation() + Vector3(0, rotation, 0))

func mouse(event):
	Yaw.set_rotation(look_leftright_rotation((event.relative.x * mouseSensitivity) / -200))
	self.set_rotation(look_updown_rotation((event.relative.y * mouseSensitivity) / -200))
	
func _input(event):
	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		return
	
	elif event is InputEventMouseMotion:
		return mouse(event)

func setFOV(newFov):
	fov = newFov
	
func zoom():
	state = 1
	if fov > FOVZoom:
		setFOV(fov - get_process_delta_time() * 100)
	
func reset(): # external function for reseting camera
	state = 0

