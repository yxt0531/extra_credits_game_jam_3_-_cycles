extends Node

# inventory variables
var cable

# state variables
var door_bathroom
var door_living_room
var cab_left
var cab_middle
var cab_right

func _ready():
	# variables init
	cable = 0
	door_bathroom = 0
	door_living_room = 0
	cab_left = 0
	cab_middle = 0
	cab_right = 0

func refreshAnimations():
	if door_bathroom != 0:
		Global.kitchen.playAnimation("open_door_bathroom")
	if door_living_room != 0:
		Global.kitchen.playAnimation("open_door_living_room")
	if cab_left != 0:
		Global.kitchen.playAnimation("open_cabinet_1")
	if cab_middle != 0:
		Global.kitchen.playAnimation("open_cabinet_2")
	if cab_right != 0:
		Global.kitchen.playAnimation("open_cabinet_3")
		
func getScript(input):
	print(input) # for debug purpose
	
	if input == "":
		return "It's a kitchen, small but enough to make a decent meal."
	if input == "look around":
		return "There is a door to bathroom and a door leads back to living room. Everything is covered in dust, the cooktop and the kitchen cabinets looks like haven't been cleaned in ages, the fan is greasy and the sink smells like rotten flesh. A ladder used for fixing water tube on ceiling, is tucked in the corner."
	if input == "look at room":
		return "It looks heavily used, as oil stains can be seen on walls."
	if input == "look at window":
		return "It is completely dark outside, you wonder what time it is now."
	if input == "open window":
		return "It's stucked, probably because it stayed shut for too long."
	
	if input == "look at ladder":
		return "You brought this to fix the leaking water tube on the kitchen ceiling, and then it becomes completely useless, wish there is another leak so you can acturally use this again."
	if input == "pick up ladder":
		return "You picked up the ladder, but it is a small kitchen, there is no other places to store it, you decide to put it back to where it was."

	if input == "look at fan":
		return "It's a ventilation fan, can be found at pretty much every kitchen on earth."
		
	if input == "look at cooktop":
		return "An electric cooktop."
		
	if input == "look at sink":
		return "It seems like it has started to rust already."

	if input == "look at bathroom door":
		return "It's a door that leads to bathroom."
	if input == "look at living room door":
		return "It's a door that goes back to living room."
	if input == "open bathroom door":
		if door_bathroom == 0:
			Global.kitchen.playAnimation("open_door_bathroom")
			door_bathroom = 1
			return "You opened the bathroom door."
		else:
			return "It is opened already."
	if input == "open living room door":
		if door_living_room == 0:
			Global.kitchen.playAnimation("open_door_living_room")
			door_living_room = 1
			return "You opened the living room door."
		else:
			return "It is opened already."
	
	if input == "look at cabinet" || input == "look at cabinets":
		return "Three simple kitchen cabinets used to store cookwares and spices."
	if input == "open cabinet" || input == "open cabinets":
		return "Do you want to open the left, middle, or right cabinet?"
	if input == "open left cabinet" || input == "open first cabinet":
		if cab_left == 0:
			Global.kitchen.playAnimation("open_cabinet_1")
			cab_left = 1
		return "There are still some spices left, you wonder if they are expired. Wait, can spice be expired or not?"
	if input == "open middle cabinet" || input == "open second cabinet":
		if cab_middle == 0:
			Global.kitchen.playAnimation("open_cabinet_2")
			cab_middle = 1
		return "Some cookware that nobody would want to have."
	if input == "open right cabinet" || input == "open last cabinet" || input == "open third cabinet":
		if cab_right == 0:
			Global.kitchen.playAnimation("open_cabinet_3")
			cab_right = 1
		return "It's empty, but if memory serves, you think you used to store all your tools in here."
	
	if input == "look at spice" || input == "look at spices":
		if cab_left == 1:
			return "Some pepper, lemongrass, thyme, you think you can make a proper steak with this, well..."
	if input == "pick up spice" || input == "pick up spices":
		if cab_left == 1:
			return "You don't want to eat, and certainly don't want to eat pepper by itself."
	
	if input == "look at cookware":
		if cab_middle == 1:
			return "A versatile dutch oven, a cast iron pan and a grill, your favorite kind of things in kitchen."
	if input == "pick up cookware":
		if cab_middle == 1:
			return "You stared at it for a little while, then you put them back to the cabinet."


	# end game options
	if input == "use ladder" || input == "hang yourself":
		cable = StoryBedroom.getInventory("cable")
		if cable == 0 && input == "use ladder":
			return "There is no leaking, no need to use it."
		elif cable == 1:
			Story.triggerEnding()
			return "You stand on top of the ladder, tie one end of the cable on your neck, another end on the water tube, then kick the ladder over. You try your best to grab the cable by your natural instinct, because of the unbearable pain caused by gravity, but only after a few seconds, you start to forget about everything.\n#gettingonline"
	
	# scene change options
	if input == "go to living room":
		if door_living_room == 1:
			Story.setLocation("living room")
			get_tree().change_scene("res://Scenes/LivingRoomScene.tscn")
		else:
			return "The door to living room is currently shut."
	if input == "go to bathroom":
		if door_bathroom == 1:
			Story.setLocation("bathroom")
			get_tree().change_scene("res://Scenes/BathroomScene.tscn")
		else:
			return "The door to bathroom is currently shut."
		
	# invalid input
	return "You don't know what that means..."
