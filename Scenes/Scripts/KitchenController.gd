extends Spatial

func _ready():
	Global.kitchen = self
	
func playAnimation(animation):
	if $AnimationPlayer.is_playing():
		$AnimationPlayer.queue(animation)
		return
	$AnimationPlayer.play(animation)
