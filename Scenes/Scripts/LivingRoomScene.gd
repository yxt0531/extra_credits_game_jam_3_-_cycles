extends Spatial

var cameraPos

func _ready():
	cameraPos = 1
	StoryLivingRoom.refreshAnimations()
	Global.livingRoomScene = self
	
func moveCamera(pos): # move camera, pos = 1 || 2 || 3
	if cameraPos != pos:
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		if cameraPos == 1:
			if pos == 2:
				$AnimationPlayer.play("cam_pos1_pos2")
				cameraPos = 2
			else:
				$AnimationPlayer.play("cam_pos1_pos3")
				cameraPos = 3
		elif cameraPos == 2:
			if pos == 1:
				$AnimationPlayer.play("cam_pos2_pos1")
				cameraPos = 1
			else:
				$AnimationPlayer.play("cam_pos2_pos3")
				cameraPos = 3
		else:
			if pos == 1:
				$AnimationPlayer.play("cam_pos3_pos1")
				cameraPos = 1
			else:
				$AnimationPlayer.play("cam_pos3_pos2")
				cameraPos = 2
	