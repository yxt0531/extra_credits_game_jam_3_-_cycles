extends Control

var black_visibility
var tempType # temperory animation type

func _ready():
	black_visibility = 0
	setVisibility(false)
	if Global.helpState == 0:
		$LabelBackground/LabelOutput.text = Story.getStory("help")
		Global.helpState = 1
	Global.gui = self
	$LineEditInput.grab_focus()

func _process(delta):
	if Input.is_action_just_pressed("ui_enter_cmd"):
		if $LineEditInput.editable != false or Story.location == "ending":
			if $LineEditInput.text == "exit game": # game exit
				get_tree().quit()
			$LabelBackground/LabelOutput.text = Story.getStory($LineEditInput.text) # pass the text in LineEdit to Story
			$LineEditInput.text = ""
		
	if Input.is_action_just_pressed("ui_cancel"): # release captured mouse
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
			$LineEditInput.grab_focus()
			
	if Input.is_action_pressed("camera_zoom"): # zoom camera
		Global.fpsCamera.zoom()
			
	if Input.is_action_just_released("camera_zoom"): # reset camera zoom
		Global.fpsCamera.reset()
		
	if Input.is_action_just_pressed("debug"): # "~" key
		pass

func _on_CaptureMouse_pressed():
	if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
		Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		$LineEditInput.grab_focus()
		
# fade black background to transparent
func fadeInBlack(type):
	if black_visibility == 0:
		tempType = type
		black_visibility = 1
		$AnimationPlayer.play("fade_in_black")
		$LineEditInput.editable = false
		
func fadeOutBlack(type):
	if black_visibility == 1:
		tempType = type
		black_visibility = 0
		$AnimationPlayer.play("fade_out_black")

func fadeInOutBlack(type): # shouldn't be used
	tempType = type
	$AnimationPlayer.play("fade_in_out")
	
func toggleFadeBlack(type):
	tempType = type
	if black_visibility == 0:
		fadeInBlack(type)
	else:
		fadeOutBlack(type)

func setVisibility(visible):
	$NonAnimatedBlackBG.visible = visible

# change scene to bedroom after any animation played, except endgame
func _on_AnimationPlayer_animation_finished(anim_name):
	if tempType == "ending":
		pass
	else:
		get_tree().change_scene("res://Scenes/BedroomScene.tscn")
