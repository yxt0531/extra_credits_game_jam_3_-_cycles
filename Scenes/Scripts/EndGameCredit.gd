extends Node2D

var slide # current slide

func _ready():
	slide = -1
	$HBoxContainer/VBoxContainer/Position.text = "Simple Little Wish"
	$HBoxContainer/VBoxContainer/Name.text = ""
	$VBoxContainer/ShortDescription.text = ""
	$VBoxContainer/ShortDescription2.text = ""
	$VBoxContainer/ShortDescription3.text = ""

func _process(delta):
	if Input.is_action_just_pressed("ui_accept"):
		_on_NextSlide_pressed()

func _on_NextSlide_pressed():
	slide = slide + 1
	if slide == 0:
		$HBoxContainer/VBoxContainer/Position.text = "Game Design"
		$HBoxContainer/VBoxContainer/Name.text = "Nono Kros"
		
	if slide == 1:
		$HBoxContainer/VBoxContainer/Position.text = "Asset Models"
	
	if slide == 2:
		$HBoxContainer/VBoxContainer/Position.text = "Script"

	if slide == 3:
		$HBoxContainer/VBoxContainer/Position.text = "Play Testing"
		$HBoxContainer/VBoxContainer/Name2.text = "Xi Wang @ Pratt Institute"
		
	if slide == 4:
		$HBoxContainer/VBoxContainer/Position.text = ""
		$HBoxContainer/VBoxContainer/Name2.text = ""
		$HBoxContainer/VBoxContainer/Name.text = ""
		$VBoxContainer/ShortDescription.text = "Feel free to contact me on:"
		$VBoxContainer/ShortDescription2.text = "yxt0531.itch.io"
		$VBoxContainer/ShortDescription3.text = "steamcommunity.com/id/yxtsteam"
		
	if slide == 5:
		$VBoxContainer/ShortDescription.text = ""
		$VBoxContainer/ShortDescription2.text = ""
		$VBoxContainer/ShortDescription3.text = ""
		$HBoxContainer/VBoxContainer/Position.text = "Thank you for playing."
		
	if slide == 6:
		Global.helpState = 0
		Story._ready()
		StoryBedroom._ready()
		StoryLivingRoom._ready()
		StoryKitchen._ready()
		StoryBathroom._ready()
		# get_tree().quit()
		get_tree().change_scene("res://Scenes/OpeningScene.tscn")
