# for debugging

extends Node

func _ready():
	pass

func getScript(input):
	print(input) # for debug purpose
	
	if input == "":
		return "# you are currently in debugging mode"

	if input == "end game":
		Story.triggerEnding()

	# gui option
	if input == "toggle fade":
		Global.gui.toggleFadeBlack("ending")
		return "# fade effect toggled"
	if input == "fade in":
		Global.gui.fadeInBlack("ending")
		return "# fading in"
	if input == "fade out":
		Global.gui.fadeOutBlack("ending")
		return "# fading out"
			
	# scene change options
	if input == "kitchen":
		get_tree().change_scene("res://Scenes/KitchenScene.tscn")
	if input == "bedroom":
		get_tree().change_scene("res://Scenes/BedroomScene.tscn")
	if input == "living room" || input == "living room" || input == "living":
		get_tree().change_scene("res://Scenes/LivingRoomScene.tscn")
	if input == "bathroom":
		get_tree().change_scene("res://Scenes/BathroomScene.tscn")	
		
	if input == "to kitchen":
		Story.setLocation("kitchen")
		get_tree().change_scene("res://Scenes/KitchenScene.tscn")
	if input == "to bedroom":
		Story.setLocation("bedroom")
		get_tree().change_scene("res://Scenes/BedroomScene.tscn")
	if input == "to living room" || input == "to living room" || input == "to living":
		Story.setLocation("living room")
		get_tree().change_scene("res://Scenes/LivingRoomScene.tscn")
	if input == "to bathroom":
		Story.setLocation("bathroom")
		get_tree().change_scene("res://Scenes/BathroomScene.tscn")	
	
	# invalid input
	return "# cmd error"
