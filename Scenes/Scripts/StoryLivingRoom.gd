extends Node

# inventory variables
var cable # can be retrived by cable = StoryBedroom.getInventory("cable")
var gun

# state variables
var door_bedroom
var door_kitchen
var picture_found
var gun_found

func _ready():
	# variables init
	cable = 0
	door_bedroom = 0
	door_kitchen = 0
	picture_found = 0
	gun_found = 0
	gun = 0
	
func refreshAnimations():
	if door_bedroom != 0:
		Global.livingRoom.playAnimation("open_door_bedroom")
	if door_kitchen != 0:
		Global.livingRoom.playAnimation("open_door_kitchen")

func getInventory(varName):
	if varName == "gun":
		return gun
	else:
		return 0

func getScript(input):
	print(input) # for debug purpose
	
	if input == "":
		Global.livingRoomScene.moveCamera(1)
		return "You are standing in the middle of your living room, it looks rather empty and lifeless, after you gave away most of your furnitures and appliances."
	if input == "look around":
		Global.livingRoomScene.moveCamera(1)
		return "There is not much left in the living room, a old simple couch you rarely sit on for the last couple months, a wooden table in the middle, a TV without anything attached to it, a small fridge, a kitchen door, a bedroom door, and an entrance door which lead to the outside of your apartment."
	
	if input == "look at room":
		return "It's your living room, looks less living nowadays."
	if input == "look at tv" || input == "look at Tv" || input == "look at TV":
		return "You used to game on this quite a lot, you seemed like gaming, but you can't recall the feeling in the past."
	if input == "pick up tv" || input == "pick up Tv" || input == "pick up TV":
		return "It's not that heavy, but you are now too weak to pick it up."
	if input == "turn on tv" || input == "turn on Tv" || input == "turn on TV":
		return "Nothing attached to it, you decide not to switch it on."
	if input == "use cable on tv" || input == "use cable on Tv" || input == "use cable on TV" || input == "connect cable to tv" || input == "connect cable to Tv" || input == "connect cable to TV":
		cable = StoryBedroom.getInventory("cable")
		if cable == 0:
			return "You don't have a cable."
		elif cable == 1:
			return "You have a better idea with the cable, you decide not to use it on TV. And there is no console for you to plug it in anyway."
	
	if input == "look at fridge":
		Global.livingRoomScene.moveCamera(2)
		return "It's a small fridge, used to store food and beverages."
	if input == "open fridge":
		Global.livingRoomScene.moveCamera(2)
		Global.livingRoom.playAnimation("open_fridge") # play open_fridge animation
		return "It's empty, you are not surprised."
	if input == "pick up fridge":
		Global.livingRoomScene.moveCamera(2)
		return "It's too heavy."
	if input == "go to fridge":
		Global.livingRoomScene.moveCamera(2)
		return "The weird throught makes you smile a little."
	
	if input == "look at entrance door":
		return "It's a door that lead to outside of your apartment."
	if input == "look at bedroom door":
		return "It's a door that lead to bedroom."
	if input == "look at kitchen door":
		return "It's a door that lead to kitchen."
		
	if input == "open entrance door":
		return "It's locked, you haven't been outside for ages, and certainly you are not planning on changing that."
	if input == "open bedroom door":
		if door_bedroom == 0:
			Global.livingRoom.playAnimation("open_door_bedroom")
		door_bedroom = 1
		return "You opened your bedroom door."
	if input == "open kitchen door":
		if door_kitchen == 0:
			Global.livingRoom.playAnimation("open_door_kitchen")
		door_kitchen = 1
		return "You opened the door to your kitchen."
	
	if input == "go outside":
		return "You can't, you are too tired to go outside."
	
	if input == "look at couch":
		return "It's a couch that belongs the owner of this apartment."
	if input == "pick up couch":
		return "There is no way you can do that, and it is not even yours."
	if input == "go to couch" || input == "sit on couch":
		Global.livingRoomScene.moveCamera(3)
		return "You sat on the couch, a though occured for a second that you may acturally miss all of these. But you immediately ditched this nonsense."
	
	if input == "search room" || input == "search couch":
		return "There is something under the couch"
		
	if input == "look under couch" || input == "search under couch":
		gun_found = 1
		return "You found a S&W Model 19 Classic, you brought this not that long ago, but never had enough courage to use it."
	if input == "look at S&W Model 19 Classic" || input == "look at gun" || input == "look at revolver":
		if gun_found == 1:
			return "It's beautiful revolver."
	if input == "pick up S&W Model 19 Classic" || input == "pick up gun" || input == "pick up revolver":
		if gun_found == 1:
			gun = 1
			return "You grip the revolver firmly in your hand."
	
	if input == "look at table":
		picture_found = 1
		return "It a table that belongs the owner of this apartment. A picture you drew a quite while ago is laying on the table."
	if input == "look at picture" && picture_found == 1:
		Global.livingRoomScene.moveCamera(3)
		return "It says 'today will be a better day~' on the back."
	if input == "pick up picture" && picture_found == 1:
		return "You can't."
	
	
	# scene change options
	if input == "go to bedroom":
		if door_bedroom == 0:
			return "The door to your bedroom is currently shut."
		elif door_bedroom == 1:
			Story.setLocation("bedroom")
			get_tree().change_scene("res://Scenes/BedroomScene.tscn")
			return "# change scene to bedroom"
	
	if input == "go to kitchen":
		if door_kitchen == 0:
			return "The door to the kitchen is currently shut."
		elif door_kitchen == 1:
			Story.setLocation("kitchen")
			get_tree().change_scene("res://Scenes/KitchenScene.tscn")
			return "# change scene to kitchen"
	
	# invalid input
	return "I don't know what that means..."
