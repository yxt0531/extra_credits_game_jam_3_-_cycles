extends Spatial

func _ready():
	Global.livingRoom = self
	
func playAnimation(animation):
	if $AnimationPlayer.is_playing():
		$AnimationPlayer.queue(animation)
		return
	$AnimationPlayer.play(animation)
