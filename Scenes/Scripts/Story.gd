extends Node

const MAX_CYCLE = 5

var location # current location of player
var cycle # current cycle
var max_step # step allowed for each cycle
var current_step
var end_game_delay

var help

func _ready():
	cycle = 0
	max_step = 35
	current_step = max_step
	end_game_delay = 1
	
	setLocation("bedroom")
	help = "Please use command similar to: look around, look at, pick up, open, go to, etc.\nObject names and commands are sometimes case sensitive.\nEnter nothing to continue, help to return to this message.\nLeft click to look around.\nBeware that some commands are not in the form stated above."
	print("# story script ready")

func setLocation(input):
	location = input
	print("# current location set: " + location)

func nextCycle():
	cycle = cycle + 1
	if cycle >= 5:
		setLocation("ending")
		current_step = 999
		cycle = 999
		Global.gui.fadeInBlack("ending")
		print("# end game triggered")
		return

	max_step = max_step - 7
	current_step = max_step
	
	Global.gui.fadeInBlack("regular")
	# setLocation("bedroom")
		
	print("# current cycle: " + str(cycle) + ", max step: " + str(max_step))

func triggerEnding():
	cycle = 4
	nextCycle()
	
func getStory(input):
	current_step = current_step - 1
	print("# remaining step: " + str(current_step))
	if current_step <= 0:
		nextCycle()
		print("# new cycle init, current cycle: " + str(cycle))
		if cycle == 999:
			return "You fall into a deep slumber.\n#nofoodformythought"
		return "You are too tired to do anything else, you decide to go to bed for now."
	
	if input == ("# 0451"): # enable debug mode
		location = "debug"
		return "# debug mode enabled"
	if location == ("debug"):
		return StoryDebugger.getScript(input)
	
	if location == ("ending"):
		if end_game_delay <= 0:
			get_tree().change_scene("res://Scenes/EndGameCredit.tscn")
		current_step = 999
		end_game_delay = end_game_delay - 1
		Global.gui.setVisibility(true)
		return "You are freed from your cycles of pain."
		
	# global story script
	if input == ("help"):
		return help # return help information
	if input == ("save me"):
		return "You used to tell your friends that you are not well, but they didn't seem to understand your feeling."
	# global story script end

	if location == ("bedroom"):
		return StoryBedroom.getScript(input)
	if location == ("living room"):
		return StoryLivingRoom.getScript(input)
	if location == ("kitchen"):
		return StoryKitchen.getScript(input)
	if location == ("bathroom"):
		return StoryBathroom.getScript(input)

	
	# invalid input caused by invalid location, should not be able to reach this line
	return "invalid location, program error, please report to dev."
	
	
