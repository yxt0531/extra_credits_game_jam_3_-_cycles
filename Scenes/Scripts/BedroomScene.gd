extends Spatial

var cameraPos

func _ready():
	cameraPos = 1
	StoryBedroom.refreshAnimations()
	Story.setLocation("bedroom")
	Global.bedroomScene = self
	
func moveCamera(pos): # move camera, pos = 1 || 2
	if cameraPos != pos:
		if Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		if cameraPos == 1:
			$AnimationPlayer.play("cam_pos1_pos2")
			cameraPos = 2
		else:
			$AnimationPlayer.play("cam_pos2_pos1")
			cameraPos = 1
	
