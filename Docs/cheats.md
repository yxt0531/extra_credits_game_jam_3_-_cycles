# All Available Command Lines

## Global

- help
- save me

## Bedroom

- look around
- look at table / cabinet / cable / manual / scissor / duct tape / pen and paper / food bag / battery / controller / bookshelf / window / bed / room / door
- open cabinet / window / door
- open first / second / third drawer
- pick up cable / manual / scissor / duct tape / pen and paper / food bag / battery / controller
- read book
- use food bag / pen and paper / scissor
- go to living room / bed
- cut wrist

## Living Room

- look around
- look at room / TV / fridge / entrance door / bedroom door / kitchen door / couch / gun / table / picture
- pick up TV / fridge / couch / gun / picture
- turn on TV
- connect cable to TV
- open fridge / entrance door / bedroom door / kitchen door
- go to fridge / bedroom / kitchen
- go outside
- sit on couch
- search room / couch
- look / search under couch

## Kitchen

- look around
- look at room / window / ladder / fan / cooktop / sink / bathroom door / living room door / cabinet / spice / cookware
- open window / bathroom door / living room door / cabinet / left cabinet / middle cabinet / right cabinet
- pick up ladder / spice / cookware
- use ladder
- go to living room / bathroom

## Bathroom

- look around
- look at room / sink / cabinet / bullet / toilet / bathtub / kitchen door
- pick up sink / bullet
- open sink / cabinet / open toilet / door
- go to cabinet / kitchen
- get in bathtub
- use gun

## Endings

### No Food for My Thought

*go to bed* at **bedroom** 5 times.

### Rose is Red, Skin is Blue

*open second drawer* at **bedroom**, *pick up scissor*, *cut wrist*

### A Breathtaking Experience

*open third drawer* at **bedroom**, *pick up food bag*, *open second drawer*, *pick up duct tape*, *use food bag*

### Getting On-Line

*open first drawer* at **bedroom**, *pick up cable*, *use ladder* at **kitchen**

### Wall Paint Decoration

*look under couch* at **living room**, *pick up gun*, *open cabinet* at **bathroom**, *pick up bullet*, *shoot myself*
