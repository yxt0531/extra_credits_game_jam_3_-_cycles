# Warning

The game may contain following:

- Suicide
- Depression
- Explicit description of suicidal act

Please seek immediate help from professionals if you are experiencing anything similar to what is depicted in game.
**National Suicide Prevention Lifeline: 1-800-273-8255**

[List of Suicide Crisis Lines from Wikipedia](https://en.wikipedia.org/wiki/List_of_suicide_crisis_lines)
